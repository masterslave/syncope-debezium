#!/bin/bash
     
cd /tmp

sed "s/\${CORE_SCHEME}/$CORE_SCHEME/" console.properties.template | 
sed "s/\${CORE_HOST}/$CORE_HOST/" | sed "s/\${CORE_PORT}/$CORE_PORT/" |
sed "s/\${ANONYMOUS_USER}/$ANONYMOUS_USER/" | sed "s/\${ANONYMOUS_KEY}/$ANONYMOUS_KEY/"  > /opt/syncope/conf/console.properties

catalina.sh run
