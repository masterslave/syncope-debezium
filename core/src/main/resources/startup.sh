#!/bin/bash
     
cd /tmp

sed "s/\${ANONYMOUS_USER}/$ANONYMOUS_USER/" security.properties.template | 
sed "s/\${ANONYMOUS_KEY}/$ANONYMOUS_KEY/" |
sed "s/\${ADMIN_USER}/$ADMIN_USER/" |
sed "s/\${ADMIN_PASSWORD}/$ADMIN_PASSWORD/" | 
sed "s/\${ADMIN_PASSWORD_ALGO}/$ADMIN_PASSWORD_ALGO/" |
sed "s/\${SECRET_KEY}/$SECRET_KEY/" | 
sed "s/\${JWS_KEY}/$JWS_KEY/" > /opt/syncope/conf/security.properties

JAVA_OPTS="-Djava.awt.headless=true -Dfile.encoding=UTF-8 -Djava.security.egd=file:/dev/./urandom -XX:+TieredCompilation -XX:TieredStopAtLevel=1"

catalina.sh run
