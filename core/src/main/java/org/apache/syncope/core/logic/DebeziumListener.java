package org.apache.syncope.core.logic;

import net.tirasa.syncope.core.kafka.message.DebeziumMessage;
import org.apache.syncope.common.lib.SyncopeConstants;
import org.apache.syncope.common.lib.patch.AttrPatch;
import org.apache.syncope.common.lib.patch.UserPatch;
import org.apache.syncope.common.lib.to.UserTO;
import org.apache.syncope.core.spring.security.AuthContextUtils;
import org.apache.syncope.common.lib.to.AttrTO;
import org.apache.syncope.common.lib.types.PatchOperation;
import org.apache.syncope.core.persistence.api.dao.UserDAO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;

@Component
public class DebeziumListener {

    private static final Logger LOG = LoggerFactory.getLogger(DebeziumListener.class);

    @Autowired
    private UserLogic userLogic;

    @Autowired
    private UserDAO userDAO;
    

    @KafkaListener(id = "${kafka.debezium.groupId}", topics = "${kafka.debezium.topic}",
            containerFactory = "kafkaDebeziumListenerContainerFactory")
    public void poll(final DebeziumMessage msg) {
        LOG.info("Received message [{}]", msg);

        AuthContextUtils.execWithAuthContext(AuthContextUtils.getDomain(), () -> {
            switch(msg.getOperation()){
                case CREATE:
                    
                    addDebeziumUser(msg);
                    break;
                    
                case DELETE:
                    
                    deleteDebeziumUser(msg);
                    break;
                    
                case UPDATE:
                    
                    updateDebeziumUser(msg);
                    break;
                default:
                    LOG.error("Messagio strano");
            }
            return null;
        });

    }

    private void addDebeziumUser(DebeziumMessage msg) {

        UserTO userTo = new UserTO();
        userTo.setRealm(SyncopeConstants.ROOT_REALM);

        userTo.setUsername(msg.getAfter().get("email").toString());

        userTo.getPlainAttrs().add(new AttrTO.Builder().schema("nome").value(msg.getAfter().get("first_name").toString()).build());
        userTo.getPlainAttrs().add(new AttrTO.Builder().schema("cognome").value(msg.getAfter().get("last_name").toString()).build());
        userLogic.create(userTo, false, false);
        LOG.info("Utente creato con successo");
            
        

    }

    private void deleteDebeziumUser(DebeziumMessage msg) {

        String key = userDAO.findKey(msg.getBefore().get("email").toString());

        if (key != null) {
            
            userLogic.delete(key, false);
             LOG.info("Delete utente OK ");
        }else{
            LOG.error("Delete, utente non tovato");
        }

    }
   
    
    private void updateDebeziumUser(DebeziumMessage msg){
        String key = userDAO.findKey(msg.getAfter().get("email").toString());

        if (key != null) {
            
            UserPatch userPatch = new UserPatch();
            userPatch.setKey(key);
            //Update  nome
            userPatch.getPlainAttrs().add(new AttrPatch.Builder().attrTO(
                    new AttrTO.Builder().schema("nome").value(msg.getAfter().get("first_name").toString()).build()).operation(PatchOperation.ADD_REPLACE).build());
            //Update userfirst_name
            userPatch.getPlainAttrs().add(new AttrPatch.Builder().attrTO(
                    new AttrTO.Builder().schema("cognome").value(msg.getAfter().get("last_name").toString()).build()).operation(PatchOperation.ADD_REPLACE).build());

            userLogic.update(userPatch, false);
            LOG.info("Update utente OK ");
        } else {
            LOG.error("Update, utente non tovato");
        }
    }
    
}
