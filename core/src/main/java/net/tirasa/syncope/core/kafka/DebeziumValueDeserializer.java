package net.tirasa.syncope.core.kafka;

import com.fasterxml.jackson.core.JsonProcessingException;
import net.tirasa.syncope.core.kafka.message.DebeziumMessage;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.NullNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import java.io.ByteArrayInputStream;
import java.nio.charset.StandardCharsets;
import java.util.Iterator;
import java.util.Map;
import net.tirasa.syncope.core.kafka.message.Operation;
import net.tirasa.syncope.core.kafka.message.Source;
import org.apache.kafka.common.serialization.Deserializer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class DebeziumValueDeserializer implements Deserializer<DebeziumMessage> {

    private static final Logger LOG = LoggerFactory.getLogger(DebeziumValueDeserializer.class);

    private static final ObjectMapper MAPPER = new ObjectMapper();

    @Override
    public void configure(final Map<String, ?> map, final boolean bln) {
    }

    @Override
    public void close() {
    }

    @Override
    public DebeziumMessage deserialize(final String string, final byte[] bytes) {
        DebeziumMessage debeziuMessage = new DebeziumMessage();
        try {
              String s = new String(bytes, StandardCharsets.UTF_8);
              if(!s.isEmpty()){

                JsonNode message = MAPPER.readTree(new ByteArrayInputStream(bytes));
                
                if (message != null) {
                    JsonNode payload = message.get("payload");

                    addInformation(payload, "before", debeziuMessage.getBefore());

                    addInformation(payload, "after", debeziuMessage.getAfter());

                    debeziuMessage.setOperation(Operation.fromOp(payload.get("op").asText()));
                    debeziuMessage.setSource(MAPPER.treeToValue(payload.get("source"), Source.class));
                } 

            } else {
                LOG.info("Empty Message");
            }

        } catch (Exception e) {
            LOG.error("Unable to parse message", e);
        }
        return debeziuMessage;
    }

    private static void addInformation(JsonNode payload, String label, Map<String, Object> map)
            throws JsonProcessingException {

        if (payload.has(label)) {
            if (!(payload.get(label) instanceof NullNode)) {
                ObjectNode info = (ObjectNode) payload.get(label);

                for (Iterator<Map.Entry<String, JsonNode>> itor = info.fields(); itor.hasNext();) {
                    Map.Entry<String, JsonNode> entry = itor.next();
                    map.put(entry.getKey(), MAPPER.treeToValue(entry.getValue(), Object.class));
                }
            }

        }
    }
}
