package net.tirasa.syncope.core.kafka.message;

import java.util.HashMap;
import java.util.Map;

public class DebeziumMessage {

    private Map<String, Object> before = new HashMap<>();

    private Map<String, Object> after = new HashMap<>();

    private Source source;

    private Operation operation;

    public Source getSource() {
        return source;
    }

    public void setSource(Source source) {
        this.source = source;
    }

    public Operation getOperation() {
        return operation;
    }

    public void setOperation(Operation operation) {
        this.operation = operation;
    }

    public Map<String, Object> getBefore() {
        return before;
    }

    public void setBefore(Map<String, Object> before) {
        this.before = before;
    }

    public Map<String, Object> getAfter() {
        return after;
    }

    public void setAfter(Map<String, Object> after) {
        this.after = after;
    }

   
    

    @Override
    public String toString() {
        return "DebeziumMessage{" + "before=" + before + ", after=" + after +
                ", source=" + source + ", operation=" + operation + '}';
    }
    
    
    
    
}
