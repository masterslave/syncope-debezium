package net.tirasa.syncope.core.kafka.message;

public enum Operation {
    CREATE("c"),
    UPDATE("u"),
    DELETE("d");

    private String op;

    Operation(final String op) {
        this.op = op;
    }

    public static Operation fromOp(String op) {
        for (Operation value : values()) {
            if (value.op.equals(op)) {
                return value;
            }
        }
        return null;
    }

}
